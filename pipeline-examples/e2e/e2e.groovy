import groovy.json.JsonSlurper
import groovy.json.JsonOutput
node {
	def mvnHome
	def artiServer
	def rtMaven
	def buildInfo
	stage('Prepare') {
		artiServer = Artifactory.server('arti-demo')
		//artiServer = Artifactory.server('artifactory-test')
		buildInfo = Artifactory.newBuildInfo()
		buildInfo.env.capture = true
		rtMaven = Artifactory.newMavenBuild()
	}
	stage('SCM') {
		git url: 'https://gitlab.com/jinriyang/project-examples.git', credentialsId: 'jinry'
	}
	//执行maven构建打包
	stage('maven build'){
		rtMaven.resolver server: artiServer, releaseRepo: 'jinry-release', snapshotRepo: 'jinry-snapshot'
		rtMaven.deployer server: artiServer, releaseRepo: 'jinry-snapshot-local', snapshotRepo: 'jinry-snapshot-local'
		rtMaven.tool = 'maven'
        rtMaven.run pom: 'maven-example/pom.xml', goals: 'clean install', buildInfo: buildInfo
		artiServer.publishBuildInfo buildInfo

	}
	stage('Sonar') {
		// Sonar scan
        def scannerHome = tool 'sonarscanner';
        withSonarQubeEnv('sonar') {
            sh "${scannerHome}/bin/sonar-scanner -Dsonar.projectKey=${JOB_NAME} -Dsonar.sources=maven-example/multi3/src -Dsonar.java.binaries=*/*/target/*"
        }
	}

//添加sonar扫描结果到包上
	stage("add sonarResult"){
		//获取最新版本号
		def curlstr="curl -uadmin:AKCp5bBreZM2Wuw2BWiX8bymHE5bbEVd6WSs2L8yALEoxtPn4UpPAbeYJJeTN1eNjgYSUKgcA \'http://jfrogchina.local:8081/artifactory/"
		def warverstr= curlstr +  "api/search/latestVersion?g=org.jfrog.test&a=multi3&repos=jinry-snapshot-local&v=7.0.1-SNAPSHOT\'";
		echo warverstr
		warVersion = ["bash","-c",warverstr].execute().text
		echo 1 + "    ：  "+warVersion
		//获取sonar扫描结果
		def getSonarIssuesCmd = "curl  GET -v http://jfrogchina.local:9000/api/issues/search?componentRoots=${JOB_NAME}";
		process = [ 'bash', '-c', getSonarIssuesCmd].execute().text

		//增加sonar扫描结果到artifactory
		def jsonSlurper = new JsonSlurper()
		def issueMap = jsonSlurper.parseText(process);
		commandText = "curl  -X PUT \"http://jfrogchina.local:8081/artifactory/api/storage/jinry-snapshot-local/org/jfrog/test/multi3/7.0.1-SNAPSHOT/multi3-"+warVersion+".war?properties=qulity.gate.sonarUrl=http://jfrogchina.local:9000/dashboard/index/${JOB_NAME};qulity.gate.sonarIssue="+issueMap.total+"\" -uadmin:AKCp5bBreZM2Wuw2BWiX8bymHE5bbEVd6WSs2L8yALEoxtPn4UpPAbeYJJeTN1eNjgYSUKgcA";
		echo commandText
		process = [ 'bash', '-c', commandText].execute().text
	}


	 //进行测试
	stage('basic test'){
		echo "add test step"
	} 

	stage('xray scan'){
		def xrayConfig = [
          'buildName'     : env.JOB_NAME,
          'buildNumber'   : env.BUILD_NUMBER,
          'failBuild'     : false
        ]
        def xrayResults = artiServer.xrayScan xrayConfig
        echo xrayResults as String
        sleep 10
	}
       
       

	//promotion操作，进行包的升级
	   stage('promotion'){
		def promotionConfig = [
			'buildName'   : buildInfo.name,
			'buildNumber' : buildInfo.number,
			'targetRepo'  : 'jinry-release-local',
			'comment': 'this is the promotion comment',
			'sourceRepo':' jinry-snapshot-local',
			'status': 'Released',
			'includeDependencies': false,
			'failFast': true,
			'copy': false
		]
		artiServer.promote promotionConfig
	}
	//进行部署
	stage('deploy') {

		def deployCmd = "ansible 127.0.0.1 -m get_url -a 'url=http://jfrogchina.local:8081/artifactory/jinry-release-local/org/jfrog/test/multi3/7.0.1-SNAPSHOT/multi3-" + warVersion+ ".war  dest=/root/apache-tomcat-8.5.34/webapps/demo.war force=true url_username=admin url_password=AKCp5bBreZM2Wuw2BWiX8bymHE5bbEVd6WSs2L8yALEoxtPn4UpPAbeYJJeTN1eNjgYSUKgcA'"
		//sh "ansible 10.1.1.1 -m shell -a '/tomcat/startup.sh' "
		echo deployCmd
		process = [ 'bash', '-c', deployCmd].execute().text
		
		commandText = "curl  -X PUT \"http://jfrogcchina.local:8081/artifactory/api/storage/jinry-release-local/org/jfrog/test/multi3/7.0.1-SNAPSHOT/multi3-"+warVersion+".war?properties=deploy.tool=ansible;deploy.env=127.0.0.1\" -uadmin:AKCp5bBreZM2Wuw2BWiX8bymHE5bbEVd6WSs2L8yALEoxtPn4UpPAbeYJJeTN1eNjgYSUKgcA";
		echo commandText
		[ 'bash', '-c', commandText].execute().text
		
		def stopCmd = "ansible 127.0.0.1 -m shell -a '/root/apache-tomcat-8.5.34/bin/shutdown.sh'"
		echo stopCmd
		[ 'bash', '-c', stopCmd].execute().text
		def startCmd = "ansible 127.0.0.1 -m shell -a '/root/apache-tomcat-8.5.34/bin/startup.sh'"
		echo startCmd
		[ 'bash', '-c', startCmd].execute().text
	}
}
